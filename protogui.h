
#ifndef _PROTOGUI_H
#define _PROTOGUI_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "windows.h"
#include "Commctrl.h"

#define _loword(v) ((unsigned short)(v))
#define _hiword(v) ((unsigned short)(((unsigned long)(v) >> 16) & 0xFFFF))

static const char PROTOGUI_VERSION[] = "0.2.0";

class Proto;
typedef void (*callback_func)(Proto*);
typedef struct CONTROL {
	int id;
	HWND handle;
	callback_func func;
};

class Proto
{

private:

	const int CONTROL_OFFSET = 10000;
	int pEventType; // event type
	int pEventControl; // control ID of event
	std::vector<CONTROL> pControls;
	std::vector<CONTROL> pWindows; // TODO: I don't remember where I was going with this
	HFONT pFont; // the default font
	HWND pHwndEvent;
	HWND pWindowId;

public:

	Proto()
	{

		pEventType = NULL;
		pEventControl = NULL;
		pHwndEvent = NULL;
		pFont = CreateFont(14, 0, 0, 0, FW_REGULAR, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, DEFAULT_PITCH, "Microsoft Sans Serif");

		INITCOMMONCONTROLSEX ct;
		ct.dwICC = ICC_TAB_CLASSES;
		ct.dwSize = sizeof(INITCOMMONCONTROLSEX);
		InitCommonControlsEx(&ct);
	};

	~Proto()
	{

		pControls.clear();

	};

	void open(std::string title, int x, int y, int w, int h)
	{

		std::ostringstream stream;
		stream << (void const*) this;
		std::string windowClass = stream.str(); //  use ptr as unique id

		WNDCLASSEX wndclass;
		wndclass.cbSize = sizeof(WNDCLASSEX);
		wndclass.style = CS_HREDRAW | CS_VREDRAW;
		wndclass.lpfnWndProc = wndproc;
		wndclass.cbClsExtra = 0;
		wndclass.cbWndExtra = 0;
		wndclass.hInstance = GetModuleHandle(0);//hInstance;
		wndclass.hIcon = LoadIcon (NULL, IDI_APPLICATION);
		wndclass.hCursor = LoadCursor (NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH) COLOR_WINDOW;
		wndclass.lpszMenuName = NULL;
		wndclass.lpszClassName = windowClass.c_str();
		wndclass.hIconSm = NULL;
		RegisterClassEx(&wndclass);

		pWindowId = CreateWindowEx(NULL, windowClass.c_str(), title.c_str(), WS_TILEDWINDOW, x, y, w, h, NULL, NULL, NULL, NULL);

		std::string d = "protog2";
		SetProp(pWindowId, d.c_str(), this ); //reference this object for wndproc

	};

	void close()
	{

		DestroyWindow(pWindowId);

	};

	void show()
	{

		ShowWindow(pWindowId, SW_SHOWDEFAULT);
		UpdateWindow(pWindowId);

	};

	void hide()
	{

		ShowWindow(pWindowId, SW_HIDE);
		UpdateWindow(pWindowId);

	};

	int addListBox(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{


		// TODO: probably should give this it's own class to condense adding items and retrieving their data
		CONTROL t;
		t.func = (callback_func) callback;
		t.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created LISTBOX with id: " << t.id <<"\n";
		t.handle = CreateWindowEx(STYLEX, "LISTBOX", text.c_str(), STYLE | LBS_NOTIFY | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) t.id, NULL, NULL);
		pControls.push_back(t);
		SendMessage(t.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return t.id;

	}

	void addListItem(int control, std::string text, int position)
	{
		SendMessage(getHandle(control), LB_INSERTSTRING, position, (LPARAM) text.c_str());
	}

	std::string getSelectedListItem(int control)
	{

		char text[256];
		HWND id = getHandle(control);
		int i = SendMessage(id, LB_GETCURSEL, 0, 0);
		SendMessage(id, LB_GETTEXT, i, (LPARAM) text);
		std::string s(text);

		return s;


	}

	int addLabel(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		CONTROL t;
		t.func = (callback_func) callback;
		t.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created LABEL with id: " << t.id <<"\n";
		t.handle = CreateWindowEx(STYLEX, "STATIC", text.c_str(), STYLE | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) t.id, NULL, NULL);
		pControls.push_back(t);
		SendMessage(t.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return t.id;

	}

	int addButton(std::string text, int x, int y, int w, int h, HWND parent = NULL, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		HWND id = parent;
		if (parent == NULL)
			id = pWindowId;

		CONTROL n;
		n.func = (callback_func) callback;
		n.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created BUTTON with id: " << n.id <<"\n";
		n.handle = CreateWindowEx(STYLEX, "BUTTON", text.c_str(), STYLE | WS_VISIBLE | WS_CHILD, x, y, w, h, id, (HMENU) n.id, NULL, NULL);
		pControls.push_back(n);
		SendMessage(n.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return n.id;

	}

	HWND addTab(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		int style_ = NULL;//WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | STYLE;
		int stylex_ = NULL;//WS_EX_CLIENTEDGE | STYLEX;

		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created TAB with id: " << c.id <<"\n";

		c.handle = CreateWindowEx(0, WC_TABCONTROL, text.c_str(), style_ | WS_CLIPSIBLINGS | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);\
		SetWindowLongPtr(c.handle, GWLP_WNDPROC, (LONG_PTR) &wndproc);

		TCITEM z;
		z.mask = TCIF_TEXT;
		z.pszText = "wat";
		TabCtrl_InsertItem(c.handle, 0, &z);
		SetWindowText(c.handle, text.c_str());

		pControls.push_back(c);
		SendMessage(c.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return c.handle;

	}

	int addInput(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		int style_ = ES_AUTOHSCROLL | STYLE;
		int stylex_ = WS_EX_CLIENTEDGE | STYLEX;

		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created INPUT with id: " << c.id <<"\n";
		c.handle = CreateWindowEx(stylex_, "EDIT", text.c_str(), style_ | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);

		SetWindowText(c.handle, text.c_str());
		pControls.push_back(c);
		SendMessage(c.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return c.id;

	}

	int addEdit(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		int style_ = WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL | STYLE;
		int stylex_ = WS_EX_CLIENTEDGE | STYLEX;
		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created EDIT with id: " << c.id <<"\n";

		c.handle = CreateWindowEx(stylex_, "EDIT", text.c_str(), style_ | WS_VISIBLE | WS_CHILD, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);
		SetWindowText(c.handle, text.c_str());

		pControls.push_back(c);
		SendMessage(c.handle, WM_SETFONT, WPARAM(pFont), TRUE);
		return c.id;

	}

	int addGroup(std::string text, int x, int y, int w, int h, void* callback = NULL, int STYLE = NULL, int STYLEX = NULL)
	{

		CONTROL c;
		c.func = (callback_func) callback;
		c.id = pControls.size() + CONTROL_OFFSET;
		std::cout << this << ": created GROUP with id: " << c.id <<"\n";

		c.handle = CreateWindowEx(STYLEX, "BUTTON", text.c_str(), STYLE | WS_VISIBLE | WS_CHILD | BS_GROUPBOX, x, y, w, h, pWindowId, (HMENU) c.id, NULL, NULL);
		SetWindowText(c.handle, text.c_str());

		pControls.push_back(c);
		return c.id;

	}

	std::string getString(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return t;

	}

	int getInt(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stoi(t);

	}

	float getFloat(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stof(t);

	}

	double getDouble(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stod(t);

	}

	long getLong(int id)
	{

		HWND handle = getHandle(id);
		int l = SendMessage(handle, WM_GETTEXTLENGTH, 0, 0);
		char* buffer = new char[l];

		SendMessage(handle, WM_GETTEXT, (WPARAM) l+1, (LPARAM) buffer);

		std::string t = buffer;
		delete buffer;

		return std::stoi(t);

	}

	int getEvent()
	{

		poll();
		int r = pEventControl;
		if (r != 0)
		{

			std::cout << this << ": received event ( " << pEventType << " ) from id: " << pEventControl << "\n";
			pEventType = NULL;
			pEventControl = NULL;

		}

		return r;

	}

private:

	bool poll()
	{

		MSG msg;

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{

			TranslateMessage(&msg);
			DispatchMessage(&msg);
			return true;

		}

		return false;

	}

	void setEvent(int event, int control)
	{

		pEventType = event;
		pEventControl = control;
		//pHwndEvent = h; // TODO: this is the the controls hwnd, not the window the control is attached to

	}

	HWND getHandle(int id)
	{

		for (std::vector<CONTROL>::iterator it = pControls.begin() ; it != pControls.end(); ++it)
		{

			if (it->id == id)
			{

				return it->handle;

			}

		}

		return 0;

	}

	static LRESULT CALLBACK wndproc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{

		Proto* t = (Proto*) GetProp(hWnd, "protog2"); // retrieve the pointer address that had the event. TODO: probably a bug because hWnd is not the necessarily the window
		t->protoWndProc(hWnd, uMsg, wParam, lParam); // call the objects version in it's place
		return DefWindowProc(hWnd, uMsg, wParam, lParam);

	};

	void protoWndProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
	{

		//std::cout << "something happened but what!?\n";
		switch(umsg)
		{


			case WM_COMMAND:

				for (std::vector<CONTROL>::iterator it = pControls.begin() ; it != pControls.end(); ++it)
				{
					int id = _loword(wparam);
					int event = _hiword(wparam);

					if (it->id == id) // iterate until we find the originating control
					{
						if (it->func != NULL) // check for a callback pointer and do stuff
						{
							it->func(this);
						}
						setEvent(event, id); //
						break;
					}
				}

				break;

			case WM_SIZE: // TODO:
				break;

			case WM_SIZING: // TODO:
				break;

			case WM_CLOSE: // TODO:
				break;

		}

};

};

#endif // _PROTOGUI_H
